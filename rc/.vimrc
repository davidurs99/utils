set encoding=utf-8          "encoding stuff
set nocompatible            "disable vi compatibility mode
set nobackup                "disable backups
set history=1000            "history lines to remember
set confirm                 "ask confirm with unsaved/ro files

set filetype=on

"""""""""""""""
" Mouse
"""""""""""""""
if $SSH_CLIENT
    "find a proper way to handle mouse selection via SSH
else
    if !has('nvim')
        set ttymouse=xterm2         "terminal mouse handling
    endif
    set mouse=a                 "use mouse
endif



"""""""""""""""
" View
"""""""""""""""
set ruler
set showcmd                 "show command in statusline

set noshowmode             "do not show mode, we user airline"
set nolist
set listchars=trail:·,precedes:«,extends:»,eol:↲,tab:▸\
set hidden                  "hide buffer when leaving


"""""""""""""""
" Edit
"""""""""""""""
set nostartofline           "keep cursor on current column with pag keys
set backspace=indent,eol,start     "use backspace in insert mode
set textwidth=89            "lines width
set wildmenu                "Show all auto-completion options
set whichwrap=<,>,h,l,[,]   "go up or down when reach end first or last char
set formatoptions-=t        "do not auto-insert newline when wrapping

"tabbing
set smarttab                "indent instead of tabbing
set autoindent              "keep indentation level on new line
set tabstop=4               "tab width
set shiftwidth=4            "indent width
set expandtab               "insert 'softtabstop' spaces

"fileType specific indentation
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2
autocmd FileType html setlocal shiftwidth=2 tabstop=2

"""""""""""""""
" Programming
"""""""""""""""
set syntax=on               "syntax highlighting
syntax on                   "syntax highlighting
"folding
set foldmethod=syntax       "fold by syntax
set foldlevelstart=20       "unfold when opening a file
set foldopen=block,hor,insert,jump,mark,percent,undo
set foldclose=              "don't fold when cursor leaves

filetype indent on          "filetype indent plugin

"http://vim.wikia.com/wiki/Fix_syntax_highlighting
autocmd BufEnter * :syntax sync ccomment


