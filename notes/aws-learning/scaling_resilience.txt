IAM :- Global Service. 
Ec2 :- Runs in specific AZ. EBS volumes stored in same specific AZ as EC2 host. Replication happens within AZ. Snapshots can be created which get replicated across AZs
S3:- Namespace is global but service is region. Any bucket created gets replicated in >=AZ within same region
R53 :- Sit close to EdgeLocations so highly resistant
ELB/ALB :- REgional Service. Enabled in specific AZ. Suggested Architecture to have ELB in all AZs hosting instances.
VPC :- Regional. Can spawn across all AZs within region. Tolerant to AZ failure. Subnet work in specific AZ. VPC-Peering/IG - HA
NATGW :- Operate in specific subnet (AZ). Implemented with redudancy. Better create NAT in each subnet
ASG :- Operate in VPC across Subnets in that VPC.
VPN :- Two IPSec endpoints working in different AZs

================================================

Stateless & Scaling Architecture

Aim for stateless always.

================================================

On-Demand vs Spot vs Reserved.

On-Demand: 
	Peak, but with no interruption.

Reserved:
	Chosen for cost reduction and base load.
	Can choose between AZ or Region
	Between 12-36 months.
	You want to use them to cover the base loads.
	Don't use for variables.

Spot:
	If you don't mind if they are terminated by AWS


================================================


Auto-Scaling 

AMI baking: Launch one instance, do your configs, and the save the image of the instance.
	Disadvantages: less flexibility
	Good for spedily launching of instances.
Bootstrapping (user data):
	Gain flexibility. 
	You loose speed. 

Monitoring enabled.

Components:
	Launch Configuration or Launch Template
		LC:
			Contains the config of the ec2 instances.
			What instances are being deployed.
			Cannot be changed while running.
		LT:
			same as LC.
			Advanced details comes only with launch template
			are modular. versioned.

	Auto Scaling Group
		Where and when instances are being deployed.

		VPC - Subnets
		Scaling Policies.
		Integration with Load Balancers.
		Health checks.
		Notifications.

		Can protect Instances. Can set to stand-by.
	Terminal Policy

================================================

ELB

CLB, ALB, NLB

Creates a Node in each AZ.
	Each node 50%
	can have cross-zone load balancing: distribute traffic to instances in other azs.
		classic: have to enable
		alb/nlb: by default

Public-facing or internal

CLB or ALB allow an SSL certificate to be applied directly to the ELB. It's called SSL Offloading. (HTTPS to load balancer, but http to servers)


CLB
	Not recommended unless EC2 classic.
	Sticky session can be enabled.
		Can be from load balancer cookies, or app cookies.
	Don't understand layer 7.

	Health Check of the AutoScalling can be changed from EC2 (default), which just checks the health of the instance, to ELB health check, which checks the health of the app.


ALB
	Understand Layer 7.
	Perform better than CLB. Cheaper than CLB.
	Can cope with multiple certificates.
	Can deal with both hosts and paths.
	Healthchecks defined at target group level.
	Supports ECS, EKS, WebSockets, Integration with WAF, HTTP/2, Lambda.
	Can redirect requests. Can authenticate.
	Target Groups. 
		Instance.
		IP addresses.
		Lambda functions.

NLB
	Operate at layer 4 (TCP layer)
	Provide extreme performance. Much higher number of transactions / second.
	Support static IP addresses. Ultra low latency.
	LNB are assigned a single IP address per AZs.

	Even if it's an encrypted connection. THe NLB doesn't interact with that.
	Uninterrupted end to end encryption.

	Can route traffic to IPs outside of the VPC.

	No UDP.

================================================


CloudFront

RTMP: only for Adobe Flash Media Server RTMP. Not for general live streaming.

Deployment architecture: 
	Deploys the configuration to all the Edge Location. It takes up to 45 minutes to create.
	Any changes will take about the same.

Behaviours
	Here you define configurations. 
	Do different actions based on behaviour
	Configure viewer protocol in the behaviour
	Example: images/*.jpg
	there is a default behaviour


Key features:
	Can link with WebACL (WAF)
	Regional cache available.
	Lambda@Edge can do compute to influence communication
	Can add SSL capability to an S3 bucket using a custom name
	If you using browsers which don't support SNI: there is an additional cost
	Security policy:
		TLSv1, v1_2016, v2_2018
		TLSv1.1_2016 (recommended)
	Can define customer error pages.
	Invalidations

Viewer Protocol
	Customise client access to edge location

Origin 
	where the content lives

How to create a Private CDN distribution:
	Use a Behavior that restricts viewer access based on signed URLs or signed cookies.

Distributions:
	= A collection of configurations.

Custom Origins:
	Not S3. Must be a public EC2 or on-premise server.
	Adds capabilities on communication between Origin and Edge Location (SSL Protocols)
	Headers can be passed.



CF Security
	Edge filtering is done by default
	2 connections
		Viewer: between clients and edge
		Oirigin: Edge and Origin
	Edge certificates: should be publicly trusted (not self-signed, not via internal certificate authority)

	Origin:
		S3 - SSL default certificate provided b S3
			SSL certificate needs to match the domain name for the origin
			OAI (Origin Access Identity) is an option.
			Can configure S3 to allow access only to OAI. Deployed to every edge location.
		LoadBalancer - Can use a cert provided by ACM
		EC2, or On-Premise: need a publicly trusted and valid certificate


	Pre-signed URL: 
		Allow an entity to access an object
		Does have an expiry
		The url gets the permissions of the one who created it

	Signed-URL and Cookies
		ALlow an entity to access an url
		Linked to an existing identity
		Not enabled by default. Gets enabled in the behaviour, not at a distribution level.
		For a behaviour: cannot have signed-urls and cookies enabled as well as public access at the same time. 
		Trusted Signers: if this is enabled, the signed-url and cookies are enabled and it's not a public behaviour
		Signed cookies don't work with RTMP distributions
		Individual objects: Signed-urls
		Whole area of objects: Signed cookies

	Restrictions:
		Geo-Restrictions: built-in at a distribution level. Can have whitelist, or blacklist. Based on the location of the IP.
		Third-party geo-restriction: will require access using signed-urls. Requires additional logic: Your app will have to use a geolocation service. Can also check for browser session, user id, etc....  Generate a signed-url if you want to grant access.

	Field level encryption:
		Allows to define a public key to a cf distribution.
		when clients are taking with edge location, the data is encrypted using this public key all the way to the origin.
		on a custom origin, you can provide a private key to decrypt that key.


Caching
	Cache hit: hitting the edge
	Cache miss: go to origin and fetch
	TTL: Minimum, Maximum, Default
		Origins could supply as a header, and override the default 

	Query string:
		Need to be aware if there are different objects
		By default, they are not forwarded to the origin
		Options:
			Forward all, cache based on all
			Forward all, cache based on a whitelist
	Cookies:
		Can be forwarded (all\whitelist), or not (default)

Lambda@Edge
	It can manipulate in-stream data between the client, edge location, and origin in a CloudFront distribution
	per behavior
	Lamdba functions:
		needs existing functions
		Invoke it based on events: Viewer request/response, Origin request/response
		It's pushed to the edge
		they can interact with the data
		functions need to be in us-east-1
	you can inspect cookies/devices/location... of the request, to deliver different content / to authenticate...etc


Logging, Reporting, Monitoring

	Can check cache statistics,Usage,Referrers, popular objects
		Globally
		By Distribution
	Can create cloudwatch alarms from any of the metrics

	Distributed logging into s3 can be enabled
	
	
================================================


Route 53

4 nameservers

Can Register Domains. Can Host domains.

Update the name servers to point to the servers from hosted zone. Done automatically if you register using R53

Health check: 
	monitor the health of endpoints. 
	can specify protocol host port path

Alias: 
	A Type: specify other aws resources 

Can use failovers, with health checks

TTL: will still point to the same value for the value of ttl until it changes.

Inbound endpoint:
	Can use to reference private route53 hosted zone. 
	Will create 2 private Elastic ip Addresses accessible from outside of the vpc.

Outbound endpoint:
	Define 2 EIP.
	Origin points for any lookups to dns names.
	

Routing:
	Simple routing policy: can have only one record but can use multiple values (delivered in a random order)

	Latency/Geolocation/Weighted
	Multivalue - will do healtchecks on all values

	You can combine different routing types